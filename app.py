from flask import Flask, render_template, request, redirect, url_for

app = Flask(__name__)

# Dummy data for job listings
jobs = []


@app.route('/')
def index():
    return render_template('index.html', jobs=jobs)


@app.route('/post_job', methods=['GET', 'POST'])
def post_job():
    if request.method == 'POST':
        title = request.form['title']
        description = request.form['description']
        company = request.form['company']
        location = request.form['location']

        job = {'title': title, 'description': description, 'company': company, 'location': location}
        jobs.append(job)

        return redirect(url_for('index'))
    return render_template('job_form.html')


if __name__ == '__main__':
    app.run(debug=True, , host='0.0.0.0')
